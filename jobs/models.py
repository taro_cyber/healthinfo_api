from django.db import models

# Create your models here.


class JobOffer(models.Model):

    recording_date = models.DateField(auto_now=False, auto_now_add=False)
    moningbody_temperature = models.CharField(max_length=5)
    nightbody_temperature = models.CharField(max_length=5,blank=True, null=True)
    nightbody_condition = models.CharField(max_length=5, blank=True, null=True)
    moningbody_condition = models.CharField(max_length=5, blank=True, null=True)
    etc = models.TextField(max_length=100, blank=True, null=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.recording_date