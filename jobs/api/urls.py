from django.urls import path
from jobs.api import views
from rest_framework_jwt.views import obtain_jwt_token

urlpatterns = [
    path('jobs/', views.ListView.as_view(), name='list'),
    path('jobs/<int:pk>/', views.DetailView.as_view(), name='detail'),
    # path('v1/token/', obtain_jwt_token, name='token_obtain_pair'),
]
